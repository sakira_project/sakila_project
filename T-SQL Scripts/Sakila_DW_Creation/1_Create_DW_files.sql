/*==============================================================*/
/* Previous removing database if there is                       */
/*==============================================================*/
USE master
GO

IF DB_ID (N'sakilaDW') IS NOT NULL
 DROP DATABASE sakilaDW;
GO
/*==============================================================*/
/* Creating Data Warehouse SakilaDW                             */
/*==============================================================*/
CREATE DATABASE [sakilaDW]
 ON PRIMARY 
( NAME = N'sakilaDW', FILENAME = N'd:\Database storage\SakilaDW\sakilaDW.mdf' , SIZE = 20MB , MAXSIZE = UNLIMITED, FILEGROWTH = 5MB )
 LOG ON 
( NAME = N'sakilaDW_log', FILENAME = N'd:\Database storage\SakilaDW\sakilaDW_log.ldf' , SIZE = 2048KB , MAXSIZE = 20GB , FILEGROWTH = 10%)
GO

-- add few additional parameters
USE [sakilaDW]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER DATABASE [sakilaDW] SET  MULTI_USER 
GO

