USE [sakilaDW];
GO

/*==============================================================*/
/* Table: BridgeFilmCategory                                    */
/*==============================================================*/
create table BridgeFilmCategory 
(
   FilmKey              smallint                       not null,
   CategoryKey          smallint                       not null,
   constraint PK_BRIDGEFILMCATEGORY primary key clustered (FilmKey, CategoryKey)
);

/*==============================================================*/
/* Table: BridgeFilming                                         */
/*==============================================================*/
create table BridgeFilming 
(
   FilmKey              smallint                       not null,
   ActorKey             smallint                       not null,
   constraint PK_BRIDGEFILMING primary key clustered (FilmKey, ActorKey)
);

/*==============================================================*/
/* Table: DimActor                                              */
/*==============================================================*/
create table DimActor 
(
   ActorKey             smallint       identity(1,1)   not null,
   ActorAltKey          smallint                       not null,
   FirstName            varchar(45)                    not null,
   LastName             varchar(45)                    not null,
   constraint PK_DIMACTOR primary key clustered (ActorKey)
);

/*==============================================================*/
/* Table: DimCategory                                           */
/*==============================================================*/
create table DimCategory 
(
   CategoryKey          smallint       identity(1,1)   not null,
   CategoryAltKey       tinyint                        not null,
   CategoryName         varchar(25)                    not null,
   constraint PK_DIMCATEGORY primary key clustered (CategoryKey)
);

/*==============================================================*/
/* Table: DimCustomer                                           */
/*==============================================================*/
create table DimCustomer 
(
   CustomerKey          integer        identity(1,1)   not null,
   CustomerAltKey       smallint                       not null,
   FirstName            varchar(45)                    not null,
   LastName             varchar(45)                    not null,
   Country              varchar(50)                    not null,
   District             varchar(20)                    null,
   City                 varchar(50)                    not null,
   constraint PK_DIMCUSTOMER primary key clustered (CustomerKey)
);

/*==============================================================*/
/* Table: DimDate                                               */
/*==============================================================*/
create table DimDate 
(
   DateKey              integer                        not null,
   DateAltKey           date                           not null,
   CalendarYear         integer                        not null,
   CalendarSemester     smallint                       not null,
   CalendarQuarter      smallint                       not null,
   MonthOfYear          smallint                       not null,
   MonthName            varchar(20)                    not null,
   WeekNumberOfMonth    smallint                       not null,
   DayOfMonth           smallint                       not null,
   DayOfWeek            smallint                       not null,
   DayName              varchar(20)                    not null,
   constraint PK_DIMDATE primary key clustered (DateKey)
);

/*==============================================================*/
/* Table: DimFilm                                               */
/*==============================================================*/
create table DimFilm 
(
   FilmKey              smallint       identity(1,1)   not null,
   FilmAltKey           smallint                       not null,
   Title                varchar(255)                   not null,
   OriginalLanguage     char(20)                       not null,
   TranslationLanguage  char(20)                       not null,
   constraint PK_DIMFILM primary key clustered (FilmKey)
);

/*==============================================================*/
/* Table: DimStaff                                              */
/*==============================================================*/
create table DimStaff 
(
   StaffKey             smallint       identity(1,1)   not null,
   StaffAltKey          tinyint                        not null,
   FirstName            varchar(45)                    not null,
   LastName             varchar(45)                    not null,
   StoreId              tinyint                        not null,
   constraint PK_DIMSTAFF primary key clustered (StaffKey)
);

/*==============================================================*/
/* Table: FactRental                                            */
/*==============================================================*/
create table FactRental 
(
   RentalId             integer                        not null,
   CustomerKey          integer                        not null,
   StaffKey             smallint                       not null,
   FilmKey              smallint                       not null,
   RentalDateKey        integer                        not null,
   ReturnDateKey        integer                        not null,
   RentalDuration       integer                        not null,
   PaymentAmount        decimal(5,2)                   not null,
   constraint PK_FACTRENTAL primary key clustered (CustomerKey, StaffKey, FilmKey, RentalDateKey, ReturnDateKey)
);

alter table BridgeFilmCategory
   add constraint FK_BRIDGEFI_REFERENCE_DIMFILM foreign key (FilmKey)
      references DimFilm (FilmKey);

alter table BridgeFilmCategory
   add constraint FK_BRIDGEFI_REFERENCE_DIMCATEG foreign key (CategoryKey)
      references DimCategory (CategoryKey);

alter table BridgeFilming
   add constraint FK_BRIDGEFI_REFERENCE_ACTOR_DIMFILM foreign key (FilmKey)
      references DimFilm (FilmKey);

alter table BridgeFilming
   add constraint FK_BRIDGEFI_REFERENCE_DIMACTOR foreign key (ActorKey)
      references DimActor (ActorKey);

alter table FactRental
   add constraint FK_FACTRENT_REFERENCE_DIMDATE_RENT_RNT foreign key (RentalDateKey)
      references DimDate (DateKey);

alter table FactRental
   add constraint FK_FACTRENT_REFERENCE_DIMDATE_RETURN_RTN foreign key (ReturnDateKey)
      references DimDate (DateKey);

alter table FactRental
   add constraint FK_FACTRENT_REFERENCE_DIMCUSTO foreign key (CustomerKey)
      references DimCustomer (CustomerKey);

alter table FactRental
   add constraint FK_FACTRENT_REFERENCE_DIMSTAFF foreign key (StaffKey)
      references DimStaff (StaffKey);

alter table FactRental
   add constraint FK_FACTRENT_REFERENCE_DIMFILM foreign key (FilmKey)
      references DimFilm (FilmKey);

