USE [sakilaDW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[LoadDimDate] 
as
begin
set language us_english;
declare @EndDate datetime;
declare @StartDate datetime;
declare @LoopDate datetime;
declare @MaxReturnDate datetime = (select max(return_date) from sakila.dbo.rental) /* the maximum return date of rental in table rental*/
 if((select MAX(DateAltKey) from sakilaDW.dbo.DimDate) is null) /* check if we have a data in table DimDate */
 begin
   set @StartDate = (select MIN(rental_date) from sakila.dbo.rental)
   if(GETDATE()<@MaxReturnDate) /* check if we have a larger return date then today's date */
   begin 
     set @EndDate = DATEADD(DAY,1,@MaxReturnDate)
   end
   else
   begin
   set @EndDate =  DATEADD(DAY,1,GETDATE())  
   end
   set @LoopDate = @StartDate
 end
 else /* if we have a data in table DimDate */
 begin
   set @StartDate = DATEADD(DAY,1,(select MAX(DateAltKey) from DimDate))
   if(GETDATE()<@MaxReturnDate) /* check if we have a larger return date then today's date */
   begin 
     set @EndDate = @MaxReturnDate
   end
   else
   begin
   set @EndDate = GETDATE()
   end
   set @LoopDate = @StartDate 
 end
 while @LoopDate <= @EndDate
 begin
   insert into dbo.DimDate values
    (
    CAST(CONVERT(varchar(10),@LoopDate,112) as int),
    @LoopDate,
    YEAR(@LoopDate),
    CASE WHEN MONTH(@LoopDate) BETWEEN 1 AND 6 THEN '1' ELSE '2' END,
    DATEPART(QQ,@LoopDate),
    MONTH(@LoopDate),
    DATENAME(MM,@LoopDate),
    DATEPART(week, @LoopDate)- DATEPART(week, CONVERT(CHAR(6), @LoopDate, 112)+'01')+ 1,
    DAY(@LoopDate),
    DATEPART(DW,@LoopDate),
    DATENAME(DW,@LoopDate)
    )
   --set @LoopDate = DATEADD(DD,1,@LoopDate)
   if @LoopDate>@EndDate
   break
   else
   set @Loopdate=DATEADD(DAY, 1, @Loopdate)
   continue
 end
end
