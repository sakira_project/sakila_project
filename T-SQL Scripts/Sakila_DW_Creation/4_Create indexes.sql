USE sakilaDW;
GO
/*indexes for FactRental*/
CREATE NONCLUSTERED INDEX Rental
ON dbo.FactRental (RentalId);
GO
/*indexes for DimStaff*/
CREATE NONCLUSTERED INDEX Staff
ON dbo.DimStaff(FirstName,LastName)
INCLUDE (StoreId);
GO
/*indexes for DimFilm*/
CREATE NONCLUSTERED INDEX Film
ON dbo.DimFilm(Title)
INCLUDE (OriginalLanguage,TranslationLanguage);
GO
/*indexes for DimDate*/
CREATE NONCLUSTERED INDEX "Date"
ON dbo.DimDate (DateAltKey)
INCLUDE (CalendarYear,"MonthName","DayName");
GO
/*indexes for DimCustomer*/
CREATE NONCLUSTERED INDEX Customer
ON dbo.DimCustomer (FirstName,LastName)
INCLUDE (Country,District,City);
GO
/*indexes for DimActor*/
CREATE NONCLUSTERED INDEX Actor
ON dbo.DimActor (FirstName,LastName)
GO
/*indexes for DimCategory*/
CREATE NONCLUSTERED INDEX Category
ON dbo.DimCategory(CategoryName)
GO