USE master
GO

IF DB_ID (N'etl') IS NOT NULL
 DROP DATABASE etl;
GO

-- create etl database
CREATE DATABASE [etl]
 ON PRIMARY 
( NAME = N'etl', FILENAME = N'd:\Database storage\Test\etl.mdf' , SIZE = 20MB , MAXSIZE = UNLIMITED, FILEGROWTH = 3MB )
 LOG ON 
( NAME = N'etl_log', FILENAME = N'd:\Database storage\Test\etl_log.ldf' , SIZE = 2048KB , MAXSIZE = 5GB , FILEGROWTH = 10%)
GO