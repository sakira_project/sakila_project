USE etl
GO

-- cdc states table
CREATE TABLE [dbo].[cdc_states] 
 ([name] [nvarchar](256) NOT NULL, 
 [state] [nvarchar](256) NOT NULL) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [cdc_states_name] ON 
 [dbo].[cdc_states] 
 ( [name] ASC ) 
 WITH (PAD_INDEX  = OFF) ON [PRIMARY]
GO


-- staging schema
CREATE SCHEMA stg
GO


-- staging for DimCategory
SELECT TOP 0 * INTO [stg].DimCategory_UPDATES
   FROM sakila.dbo.category

SELECT TOP 0 * INTO [stg].DimCategory_DELETES
   FROM sakila.dbo.category
GO


-- staging for DimActor
SELECT TOP 0 * INTO [stg].DimActor_UPDATES
   FROM sakila.dbo.actor

SELECT TOP 0 * INTO [stg].DimActor_DELETES
   FROM sakila.dbo.actor
GO


-- staging for DimStaff
SELECT TOP 0 * INTO [stg].DimStaff_UPDATES
   FROM sakila.dbo.staff

SELECT TOP 0 * INTO [stg].DimStaff_DELETES
   FROM sakila.dbo.staff
GO


-- staging for DimFilm
SELECT TOP 0 * INTO [stg].DimFilm_UPDATES
   FROM sakila.dbo.film;

SELECT TOP 0 * INTO [stg].DimFilm_DELETES
   FROM sakila.dbo.film;
GO


-- staging for DimCustomer
SELECT TOP 0 * INTO [stg].DimCustomer_UPDATES
   FROM sakila.dbo.customer;

SELECT TOP 0 * INTO [stg].DimCustomer_DELETES
   FROM sakila.dbo.customer;
GO


-- staging for BridgeFilmCategory
SELECT TOP 0 * INTO [stg].BridgeFilmCategory_UPDATES
   FROM sakila.dbo.film_category;

SELECT TOP 0 * INTO [stg].BridgeFilmCategory_DELETES
   FROM sakila.dbo.film_category;
GO


-- staging for BridgeFilming
SELECT TOP 0 * INTO [stg].BridgeFilming_UPDATES
   FROM sakila.dbo.film_actor;

SELECT TOP 0 * INTO [stg].BridgeFilming_DELETES
   FROM sakila.dbo.film_actor;
GO


-- staging for FactRental
SELECT TOP 0 * INTO [stg].FactRental_UPDATES
   FROM sakila.dbo.rental;

SELECT TOP 0 * INTO [stg].FactRental_DELETES
   FROM sakila.dbo.rental;
GO

SELECT TOP 0 * INTO [stg].FactRental_INSERTS
   FROM sakila.dbo.rental;
GO

SELECT TOP 0 * INTO [etl].[stg].FactRental_TMP
   FROM sakilaDW.dbo.FactRental;
GO


-- staging for customer geography
CREATE TABLE [etl].[stg].Geography_Address_UPDATES(
	district VARCHAR(20) NOT NULL,
	old_district VARCHAR(20) NOT NULL);
GO


CREATE TABLE [etl].[stg].Geography_City_UPDATES(
	city VARCHAR(50) NOT NULL,
	old_city VARCHAR(50) NOT NULL);
GO


CREATE TABLE [etl].[stg].Geography_Country_UPDATES(
	country VARCHAR(50) NOT NULL,
	old_country VARCHAR(50) NOT NULL);
GO