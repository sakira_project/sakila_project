USE [sakila]
GO
/****** Object:  Table [dbo].[actor]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[actor](
	[actor_id] [smallint] NOT NULL,
	[first_name] [varchar](45) NOT NULL,
	[last_name] [varchar](45) NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[actor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[address]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[address](
	[address_id] [smallint] NOT NULL,
	[address] [varchar](50) NOT NULL,
	[address2] [varchar](50) NULL,
	[district] [varchar](20) NOT NULL,
	[city_id] [smallint] NOT NULL,
	[postal_code] [varchar](10) NULL,
	[phone] [varchar](20) NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[category]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[category](
	[category_id] [tinyint] NOT NULL,
	[name] [varchar](25) NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[city]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[city](
	[city_id] [smallint] NOT NULL,
	[city] [varchar](50) NOT NULL,
	[country_id] [smallint] NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[city_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[country]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[country](
	[country_id] [smallint] NOT NULL,
	[country] [varchar](50) NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[country_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[customer]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customer](
	[customer_id] [smallint] NOT NULL,
	[store_id] [tinyint] NOT NULL,
	[first_name] [varchar](45) NOT NULL,
	[last_name] [varchar](45) NOT NULL,
	[email] [varchar](50) NULL,
	[address_id] [smallint] NOT NULL,
	[active] [bit] NOT NULL,
	[create_date] [datetime] NOT NULL,
	[last_update] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[film]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[film](
	[film_id] [smallint] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[release_year] [int] NULL,
	[language_id] [tinyint] NOT NULL,
	[original_language_id] [tinyint] NULL,
	[rental_duration] [tinyint] NOT NULL,
	[rental_rate] [decimal](4, 2) NOT NULL,
	[length] [smallint] NULL,
	[replacement_cost] [decimal](5, 2) NOT NULL,
	[rating] [varchar](50) NULL,
	[special_features] [varchar](255) NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[film_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[film_actor]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[film_actor](
	[actor_id] [smallint] NOT NULL,
	[film_id] [smallint] NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[actor_id] ASC,
	[film_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[film_category]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[film_category](
	[film_id] [smallint] NOT NULL,
	[category_id] [tinyint] NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[film_id] ASC,
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory](
	[inventory_id] [int] NOT NULL,
	[film_id] [smallint] NOT NULL,
	[store_id] [tinyint] NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[language]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[language](
	[language_id] [tinyint] NOT NULL,
	[name] [char](20) NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[language_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[payment]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payment](
	[payment_id] [smallint] NOT NULL,
	[customer_id] [smallint] NOT NULL,
	[staff_id] [tinyint] NOT NULL,
	[rental_id] [int] NULL,
	[amount] [decimal](5, 2) NOT NULL,
	[payment_date] [datetime] NOT NULL,
	[last_update] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[payment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[rental]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rental](
	[rental_id] [int] NOT NULL,
	[rental_date] [datetime] NOT NULL,
	[inventory_id] [int] NOT NULL,
	[customer_id] [smallint] NOT NULL,
	[return_date] [datetime] NULL,
	[staff_id] [tinyint] NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[rental_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[staff]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[staff](
	[staff_id] [tinyint] NOT NULL,
	[first_name] [varchar](45) NOT NULL,
	[last_name] [varchar](45) NOT NULL,
	[address_id] [smallint] NOT NULL,
	[picture] [varbinary](max) NULL,
	[email] [varchar](50) NULL,
	[store_id] [tinyint] NOT NULL,
	[active] [bit] NOT NULL,
	[username] [varchar](16) NOT NULL,
	[password] [varchar](40) NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[staff_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[store]    Script Date: 1/15/2014 11:36:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[store](
	[store_id] [tinyint] NOT NULL,
	[manager_staff_id] [tinyint] NOT NULL,
	[address_id] [smallint] NOT NULL,
	[last_update] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[store_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




-- actor table
INSERT INTO [sakila].[dbo].[actor]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[actor];


-- address table
INSERT INTO [sakila].[dbo].[address]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[address];


-- category table
INSERT INTO [sakila].[dbo].[category]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[category];


-- city table
INSERT INTO [sakila].[dbo].[city]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[city];


-- country table
INSERT INTO [sakila].[dbo].[country]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[country];


-- customer table
INSERT INTO [sakila].[dbo].[customer]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[customer];


-- film table
INSERT INTO [sakila].[dbo].[film]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[film];


-- film_actor table
INSERT INTO [sakila].[dbo].[film_actor]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[film_actor];


-- film_category
INSERT INTO [sakila].[dbo].[film_category]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[film_category];


-- inventory table
INSERT INTO [sakila].[dbo].[inventory]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[inventory];


-- language table
INSERT INTO [sakila].[dbo].[language]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[language];


-- payment table
INSERT INTO [sakila].[dbo].[payment]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[payment];


-- rental table
INSERT INTO [sakila].[dbo].[rental]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[rental];


-- staff table
INSERT INTO [sakila].[dbo].[staff]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[staff];


-- store table
INSERT INTO [sakila].[dbo].[store]
SELECT * 
   FROM [BACKUP_sakila].[dbo].[store];


ALTER TABLE [dbo].[actor] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[address] ADD  DEFAULT (NULL) FOR [address2]
GO
ALTER TABLE [dbo].[address] ADD  DEFAULT (NULL) FOR [postal_code]
GO
ALTER TABLE [dbo].[address] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[category] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[city] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[country] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[customer] ADD  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[customer] ADD  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[customer] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT (NULL) FOR [description]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT (NULL) FOR [release_year]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT (NULL) FOR [original_language_id]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT ((3)) FOR [rental_duration]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT ((4.99)) FOR [rental_rate]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT (NULL) FOR [length]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT ((19.99)) FOR [replacement_cost]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT ('G') FOR [rating]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT (NULL) FOR [special_features]
GO
ALTER TABLE [dbo].[film] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[film_actor] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[film_category] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[inventory] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[language] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[payment] ADD  DEFAULT (NULL) FOR [rental_id]
GO
ALTER TABLE [dbo].[payment] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[rental] ADD  DEFAULT (NULL) FOR [return_date]
GO
ALTER TABLE [dbo].[rental] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[staff] ADD  DEFAULT (NULL) FOR [picture]
GO
ALTER TABLE [dbo].[staff] ADD  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[staff] ADD  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[staff] ADD  DEFAULT (NULL) FOR [password]
GO
ALTER TABLE [dbo].[staff] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[store] ADD  DEFAULT (getdate()) FOR [last_update]
GO
ALTER TABLE [dbo].[address]  WITH CHECK ADD  CONSTRAINT [fk_address_city] FOREIGN KEY([city_id])
REFERENCES [dbo].[city] ([city_id])
GO
ALTER TABLE [dbo].[address] CHECK CONSTRAINT [fk_address_city]
GO
ALTER TABLE [dbo].[city]  WITH CHECK ADD  CONSTRAINT [fk_city_country] FOREIGN KEY([country_id])
REFERENCES [dbo].[country] ([country_id])
GO
ALTER TABLE [dbo].[city] CHECK CONSTRAINT [fk_city_country]
GO
ALTER TABLE [dbo].[customer]  WITH CHECK ADD  CONSTRAINT [fk_customer_address] FOREIGN KEY([address_id])
REFERENCES [dbo].[address] ([address_id])
GO
ALTER TABLE [dbo].[customer] CHECK CONSTRAINT [fk_customer_address]
GO
ALTER TABLE [dbo].[customer]  WITH CHECK ADD  CONSTRAINT [fk_customer_store] FOREIGN KEY([store_id])
REFERENCES [dbo].[store] ([store_id])
GO
ALTER TABLE [dbo].[customer] CHECK CONSTRAINT [fk_customer_store]
GO
ALTER TABLE [dbo].[film]  WITH CHECK ADD  CONSTRAINT [fk_film_language] FOREIGN KEY([language_id])
REFERENCES [dbo].[language] ([language_id])
GO
ALTER TABLE [dbo].[film] CHECK CONSTRAINT [fk_film_language]
GO
ALTER TABLE [dbo].[film]  WITH CHECK ADD  CONSTRAINT [fk_film_language_original] FOREIGN KEY([original_language_id])
REFERENCES [dbo].[language] ([language_id])
GO
ALTER TABLE [dbo].[film] CHECK CONSTRAINT [fk_film_language_original]
GO
ALTER TABLE [dbo].[film_actor]  WITH CHECK ADD  CONSTRAINT [fk_film_actor_actor] FOREIGN KEY([actor_id])
REFERENCES [dbo].[actor] ([actor_id])
GO
ALTER TABLE [dbo].[film_actor] CHECK CONSTRAINT [fk_film_actor_actor]
GO
ALTER TABLE [dbo].[film_actor]  WITH CHECK ADD  CONSTRAINT [fk_film_actor_film] FOREIGN KEY([film_id])
REFERENCES [dbo].[film] ([film_id])
GO
ALTER TABLE [dbo].[film_actor] CHECK CONSTRAINT [fk_film_actor_film]
GO
ALTER TABLE [dbo].[film_category]  WITH CHECK ADD  CONSTRAINT [fk_film_category_category] FOREIGN KEY([category_id])
REFERENCES [dbo].[category] ([category_id])
GO
ALTER TABLE [dbo].[film_category] CHECK CONSTRAINT [fk_film_category_category]
GO
ALTER TABLE [dbo].[film_category]  WITH CHECK ADD  CONSTRAINT [fk_film_category_film] FOREIGN KEY([film_id])
REFERENCES [dbo].[film] ([film_id])
GO
ALTER TABLE [dbo].[film_category] CHECK CONSTRAINT [fk_film_category_film]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_inventory_film] FOREIGN KEY([film_id])
REFERENCES [dbo].[film] ([film_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_inventory_film]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_inventory_store] FOREIGN KEY([store_id])
REFERENCES [dbo].[store] ([store_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_inventory_store]
GO
ALTER TABLE [dbo].[payment]  WITH CHECK ADD  CONSTRAINT [fk_payment_customer] FOREIGN KEY([customer_id])
REFERENCES [dbo].[customer] ([customer_id])
GO
ALTER TABLE [dbo].[payment] CHECK CONSTRAINT [fk_payment_customer]
GO
ALTER TABLE [dbo].[payment]  WITH CHECK ADD  CONSTRAINT [fk_payment_rental] FOREIGN KEY([rental_id])
REFERENCES [dbo].[rental] ([rental_id])
GO
ALTER TABLE [dbo].[payment] CHECK CONSTRAINT [fk_payment_rental]
GO
ALTER TABLE [dbo].[payment]  WITH CHECK ADD  CONSTRAINT [fk_payment_staff] FOREIGN KEY([staff_id])
REFERENCES [dbo].[staff] ([staff_id])
GO
ALTER TABLE [dbo].[payment] CHECK CONSTRAINT [fk_payment_staff]
GO
ALTER TABLE [dbo].[rental]  WITH CHECK ADD  CONSTRAINT [fk_rental_customer] FOREIGN KEY([customer_id])
REFERENCES [dbo].[customer] ([customer_id])
GO
ALTER TABLE [dbo].[rental] CHECK CONSTRAINT [fk_rental_customer]
GO
ALTER TABLE [dbo].[rental]  WITH CHECK ADD  CONSTRAINT [fk_rental_inventory] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[rental] CHECK CONSTRAINT [fk_rental_inventory]
GO
ALTER TABLE [dbo].[rental]  WITH CHECK ADD  CONSTRAINT [fk_rental_staff] FOREIGN KEY([staff_id])
REFERENCES [dbo].[staff] ([staff_id])
GO
ALTER TABLE [dbo].[rental] CHECK CONSTRAINT [fk_rental_staff]
GO
ALTER TABLE [dbo].[staff]  WITH CHECK ADD  CONSTRAINT [fk_staff_address] FOREIGN KEY([address_id])
REFERENCES [dbo].[address] ([address_id])
GO
ALTER TABLE [dbo].[staff] CHECK CONSTRAINT [fk_staff_address]
GO
ALTER TABLE [dbo].[staff]  WITH CHECK ADD  CONSTRAINT [fk_staff_store] FOREIGN KEY([store_id])
REFERENCES [dbo].[store] ([store_id])
GO
ALTER TABLE [dbo].[staff] CHECK CONSTRAINT [fk_staff_store]
GO
ALTER TABLE [dbo].[store]  WITH CHECK ADD  CONSTRAINT [fk_store_address] FOREIGN KEY([address_id])
REFERENCES [dbo].[address] ([address_id])
GO
ALTER TABLE [dbo].[store] CHECK CONSTRAINT [fk_store_address]
GO