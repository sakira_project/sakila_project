use sakila
go

-- trigger for FilmCategory
create trigger [dbo].[GetFilmCategoryUpdates]
on [sakila].[dbo].[film_category]
for update
as
	begin
		if((select count(*) from deleted) IS NOT NULL)
		begin
		insert into etl.stg.BridgeFilmCategory_UPDATES(film_id, category_id, last_update)
		select film_id, category_id, GETDATE()
		from deleted
		end
	end
go

-- trigger for Filming
create trigger [dbo].[GetFilmingUpdates]
on [sakila].[dbo].[film_actor]
for update
as
	begin
		if((select count(*) from deleted) IS NOT NULL)
		begin
		insert into etl.stg.BridgeFilming_UPDATES(actor_id, film_id, last_update)
		select actor_id, film_id, GETDATE()
		from deleted
		end
	end
go


-- triggers for Fact Table
create trigger [dbo].[GetRentalInsert]
on [sakila].[dbo].[rental]
for insert
as
	begin
		insert into etl.stg.FactRental_INSERTS
		select *
		from inserted
	end
go

create trigger [dbo].[GetRentalDelete]
on [sakila].[dbo].[rental]
for delete
as
	begin
		insert into etl.stg.FactRental_DELETES
		select *
		from deleted
	end
go

create trigger [dbo].[GetRentalUpdate]
on [sakila].[dbo].[rental]
for update
as
	begin
		insert into etl.stg.FactRental_UPDATES
		select *
		from inserted d
	end
go


-- triggers for customer geogpraphy updates
create trigger [dbo].[GetDistrictUpdate]
on [sakila].[dbo].[address]
for update
as
	begin
		insert into etl.stg.Geography_Address_UPDATES(old_district, district)
		values((select district from deleted), (select district from inserted));
	end
go

create trigger [dbo].[GetCityUpdate]
on [sakila].[dbo].[city]
for update
as
	begin
		insert into etl.stg.Geography_City_UPDATES(old_city, city)
		values((select city from deleted), (select city from inserted));
	end
go

create trigger [dbo].[GetCountryUpdate]
on [sakila].[dbo].[country]
for update
as
	begin
		insert into etl.stg.Geography_Country_UPDATES(old_country, country)
		values((select country from deleted), (select country from inserted));
	end
go