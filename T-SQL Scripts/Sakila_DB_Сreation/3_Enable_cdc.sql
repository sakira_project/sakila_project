USE sakila
GO

-- enable database cdc
EXEC sys.sp_cdc_enable_db
GO

-- enable cdc for "Rental" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'rental',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Staff" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'staff',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Actor" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'actor',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Category" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'category',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Customer" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'customer',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Payment" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'payment',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Film" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'film',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Address" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'address',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "City" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'city',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Country" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'country',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Film_Actor" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'film_actor',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO

-- enable cdc for "Film_Category" table
EXEC sys.sp_cdc_enable_table
	@source_schema = N'dbo',
	@source_name = N'film_category',
	@role_name = N'cdc_admin',
	@supports_net_changes = 1
GO