use master
go

create database sakilaLOG;
go

use sakilaLOG
go

create table logs(
	id int identity(1,1) not null primary key,
	package varchar(200) not null,
	event_time datetime not null,
	msg_type varchar(15) not null,
	code int not null,
	msg varchar(max) not null
	);

-- create table for database structure
select top 0 * into problem_payments
from sakila.dbo.payment
go