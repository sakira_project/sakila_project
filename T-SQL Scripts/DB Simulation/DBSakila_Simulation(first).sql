--first inserts into database
-- Using database Sakila
use sakila
go

-- Inserts actors into database's table Actor
declare @row int = (select max(actor_id) from dbo.actor);
INSERT INTO actor([actor_id],[first_name],[last_name]) 
	VALUES( @row+1,'Martina','Burke'),
	(@row+2,'Randall','Wall'),
	(@row+3,'Malachi','Powell'),
	(@row+4,'Quinlan','Vega'),
	(@row+5,'Alice','Leonard'),
	(@row+06,'Imani','Hayes'),
	(@row+7,'Ivan','Nieves'),
	(@row+8,'Olympia','Middleton'),
	(@row+9,'Martina','Dillon'),
	(@row+10,'Whilemina','Le'),
	(@row+11,'Dexter','Obrien'),
	(@row+12,'Elmo','Kerr'),
	(@row+13,'Medge','Curry'),
	(@row+14,'Silas','Ruiz'),
	(@row+15,'Kiara','Calhoun'),
	(@row+16,'Samuel','Jones'),
	(@row+17,'Erasmus','Martin'),
	(@row+18,'Raja','Bennett'),
	(@row+19,'Adria','Ray'),
	(@row+20,'Griffith','Albert'),
	(@row+21,'Nora','Mcfarland'),
	(@row+22,'Castor','Griffin'),
	(@row+23,'Dara','Mcmahon'),
	(@row+24,'Catherine','Mcbride'),
	(@row+25,'Casey','Ball'),
	(@row+26,'Kendall','Walter'),
	(@row+27,'Warren','Howe'),
	(@row+28,'Bell','Byers'),
	(@row+29,'Erica','Mercado'),
	(@row+30,'Natalie','Shelton'),
	(@row+31,'Chancellor','Mueller'),
	(@row+32,'Jared','Barton'),
	(@row+33,'Rudyard','Bryant'),
	(@row+34,'Iona','Stafford'),
	(@row+35,'Leonard','Good'),
	(@row+36,'Ross','Warren'),
	(@row+37,'Maxine','Hall'),
	(@row+38,'Kay','Payne'),
	(@row+39,'Emma','Waller'),
	(@row+40,'Desiree','Melendez'),
	(@row+41,'Maya','Hoover'),
	(@row+42,'Leo','Cohen'),
	(@row+43,'Gareth','Bernard'),
	(@row+44,'Tamara','Reed'),
	(@row+45,'Josiah','Barry'),
	(@row+46,'Reuben','Morse'),
	(@row+47,'Lois','Hyde'),
	(@row+48,'Bert','Rosa'),
	(@row+49,'Ulysses','Chandler'),
	(@row+50,'Montana','Reynolds');
Print 'Actor ready'
GO

-- Inserts staff into database's table Staff
declare @key int = (select max(staff_id) from dbo.staff) + 1;
insert into staff(staff_id, first_name, last_name, address_id, store_id, active, username)
	values(@key, 'Taras', 'Khoma', 11, 2, 1, 'TarasKhoma'),
	(@key+1, 'Iryna', 'Redsav', 11, 2, 1, 'IrynaRedsav'),
	(@key+2, 'Oleh', 'Borys', 11, 2, 1, 'OlehBorys'),
	(@key+3, 'Igor', 'Mifur', 11, 2, 1, 'IgorMifur');
Print 'Staff ready'
GO 

-- Inserts films into database's table Film
declare @keyFilm int = (select max(film_id) from dbo.film) + 1;
insert into film(film_id, title, language_id, original_language_id, rental_duration, rental_rate, replacement_cost)
	values(@keyFilm, 'The Grey', 1, 3, 9, 4, 20),
	(@keyFilm+1, 'This Means War', 1, 2, 5, 5, 16),
	(@keyFilm+2, 'Secret of Eden', 2, 4, 15, 3, 17),
	(@keyFilm+3, 'Journey 2: The Mysterious Island', 1, 3, 8, 7, 11),
	(@keyFilm+4, 'Contraband', 3, 5, 11, 6, 13),
	(@keyFilm+5, 'Battleship', 1, 1, 12, 5, 12);
Print 'film ready'
Go

-- Insert categories into database's table Category
declare @RowCategory int = (select max(category_id) from dbo.category) + 1;
INSERT INTO category([category_id],[name]) 
	VALUES(@RowCategory,'Art'),
	(@RowCategory+1,'Adventure'),
	(@RowCategory+2,'biographical'),
	(@RowCategory+3,'dance'),
	(@RowCategory+4,'psychological'),
	(@RowCategory+5,'historical'),
	(@RowCategory+6,'political'),
	(@RowCategory+7,'Fantasy');
Print 'Category ready'
GO

-- Inserts stores into database's table Store
declare @RowStore int = (select max(store_id) from dbo.store) + 1;
INSERT INTO store([store_id],[manager_staff_id],[address_id]) 
	VALUES(@RowStore,3,453),
	(@RowStore+1,4,234),
	(@RowStore+2,5,4);
	Print 'Store ready'
GO

-- Inserts customers into database's table Customer
declare @RowCustomer int = (select max(customer_id) from dbo.customer) + 1;
INSERT INTO customer([customer_id],[store_id],[first_name], [last_name], email, active, address_id, create_date) 
	VALUES(@RowCustomer,3,'Lucy','Middleton','ultricies.dignissim@pedeblanditcongue.co.uk',1, 4,'2009-02-14 22:04:36.000'),
	(@RowCustomer+1,1,'Tara','Knox','vitae.diam.Proin@eu.com',1,6,'2015-08-14 22:04:36.000'),
	(@RowCustomer+2,2,'Aristotle','Grimes','turpis.nec@eu.org',0,543,'2009-02-18 10:04:36.000'),
	(@RowCustomer+3,4,'Anjolie','Bond','turpis@dolor.edu',1,342,'2014-02-14 13:15:36.000'),
	(@RowCustomer+4,5,'Adria','Mcbride','mus@mollislectus.org',1,75, '2015-05-20 13:20:36.000'),
	(@RowCustomer+5,2,'Kasimir','Oneil','auctor.nunc.nulla@Nulla.net',0,34,'2015-05-20 13:20:36.000'),
	(@RowCustomer+6,1,'Yasir','Bishop','augue.ut.lacus@iaculisenimsit.edu',1,87,'2015-06-17 17:59:59.000');
Print 'Customer ready'
GO

-- Inserts films and it's categories into database's table film category
fc_generator:
declare @film int = ROUND((((select max(film_id) from dbo.film) - 1 -1) * RAND() + 1), 0);
if(@film not in(select film_id from film)) goto fc_generator;


declare @category int = ROUND((((select max(category_id) from dbo.category) - 1 -1) * RAND() + 1), 0);
if(@category not in (select category_id from category)) goto fc_generator;

if(@category in (select category_id from film_category where film_id = @film)) goto fc_generator;
if(@film in (select film_id from film_category where category_id = @category)) goto fc_generator;

insert into film_category([film_id],[category_id])
VALUES (@film,@category);
GO 6

Print 'References between Films and it`s categories ready'
GO

-- Inserts films and it's actors into database's table film actor
act_film_generator:

DECLARE @Random INT;
DECLARE @RandomF INT;
DECLARE @Upper INT;
DECLARE @Lower INT;
DECLARE @UpperF INT;
declare @i int=1;

  ---- This will create a random number between 1 and count of rows in table Actor
SET @Lower = 1 ---- The lowest random number
SET @Upper = (select max(actor_id) from dbo.actor) ---- The highest random number(count of actors in table Actor)
SET @UpperF = (select max(film_id) from dbo.film); ---- The highest random number(count of actors in table Film)

	while @i< 2-- Adding 2 random actors to random film
		begin
			SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0);			
			SELECT @RandomF = ROUND(((@UpperF - @Lower -1) * RAND() + @Lower), 0);
			
			-- check constraints
			if(@RandomF IN (select film_id from film_actor where actor_id = @Random)) goto act_film_generator;
			if(@Random NOT IN (select actor_id  from actor)) goto act_film_generator;
			if(@Random IN (select actor_id from film_actor where film_id = @RandomF)) goto act_film_generator;
			if(@RandomF NOT IN (select film_id  from film)) goto act_film_generator;

			-- insert checked value
			insert into film_actor(film_id, actor_id)
				values (@RandomF, @Random);
			Set @i= @i+1
		end
Print 'References between Films and it`s Actors ready'
GO

-- Inserts payments into database's table Payment and Rental
--1
declare @key int = (select max(rental_id) from dbo.rental) + 1;
declare @pkey int = (select max(payment_id) from dbo.payment) + 1;


INSERT INTO [dbo].[rental]([rental_id],[rental_date],[inventory_id],[customer_id],[return_date],[staff_id],[last_update])
     VALUES(@key,'2007-05-28 01:18:56.000',917,596,'2009-06-01 01:24:56.000',1,'2006-03-15 21:30:53.000')

INSERT INTO [dbo].[payment]([payment_id],[customer_id],[staff_id],[rental_id],[amount],[payment_date],[last_update])
     VALUES(@pkey,599,2,@key,4.99,'2007-05-28 01:18:56.000','2006-02-15 22:24:09.000')
GO
Print 'Payment and Rental 1 ready'
--2
declare @key int = (select max(rental_id) from dbo.rental) + 1;
declare @pkey int = (select max(payment_id) from dbo.payment) + 1;


INSERT INTO [dbo].[rental]
           ([rental_id],[rental_date],[inventory_id],[customer_id],[return_date],[staff_id],[last_update])
     VALUES(@key,'2008-05-28 01:18:56.000',917,596,'2009-06-01 01:24:56.000',1,'2006-03-15 21:30:53.000')

INSERT INTO [dbo].[payment]([payment_id],[customer_id],[staff_id],[rental_id],[amount],[payment_date],[last_update])
     VALUES(@pkey,599,2,@key,4.99,'2008-05-28 01:18:56.000','2006-02-15 22:24:09.000')
GO
Print 'Payment and Rental 2 ready'
--3
declare @key int = (select max(rental_id) from dbo.rental) + 1;
declare @pkey int = (select max(payment_id) from dbo.payment) + 1;


INSERT INTO [dbo].[rental]
           ([rental_id],[rental_date],[inventory_id],[customer_id],[return_date],[staff_id],[last_update])
     VALUES(@key,'2008-07-28 01:56:56.000',906,506,'2009-04-01 18:24:56.000',1,'2009-05-15 21:30:53.000')

INSERT INTO [dbo].[payment]([payment_id],[customer_id],[staff_id],[rental_id],[amount],[payment_date],[last_update])
     VALUES(@pkey,506,3,@key,15.99,'2008-05-28 01:18:56.000','2006-02-15 22:24:09.000')
GO
Print 'Payment and Rental 3 ready'