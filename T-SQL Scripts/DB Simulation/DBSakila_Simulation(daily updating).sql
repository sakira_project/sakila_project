--simulation of daily updating database
use sakila
go

-- Inserts 40 actors into table Actor
-- Create the variables for the random number generation

IF((SELECT COUNT(*) FROM dbo.actor)<31000)
BEGIN
DECLARE @Random INT;
DECLARE @Upper INT;
DECLARE @Lower INT

-- This will create a random number between 1 and count of rows in table Actor
SET @Lower = 2 ---- The lowest random number
SET @Upper = (SELECT max(actor_id) FROM dbo.actor) ---- The highest random number
DECLARE @row INT = (SELECT max(actor_id) FROM dbo.actor) + 1; -- number of INSERTing row

-- Generate name and lastname for insert
name_generator:
SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
if(@Random NOT IN (SELECT actor_id  FROM actor)) GOTO name_generator;

DECLARE @name VARCHAR(25) = (SELECT first_name FROM actor WHERE actor_id=@Random);

lname_generator:
SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
if(@Random NOT IN (SELECT actor_id  FROM actor)) GOTO lname_generator;

DECLARE @lname VARCHAR(25) = (SELECT last_name FROM actor WHERE actor_id=@Random);

-- Insert row
INSERT INTO actor([actor_id],[first_name],[last_name]) 
	VALUES(@row,@name,@lname);

PRINT 'Inserts actors into database table Actor';
END
GO 40


--Delete some actors from database
DECLARE @Del1 INT;
DECLARE @Del2 INT;
DECLARE @Upper INT;
DECLARE @Lower INT

SET @Lower = 1 ---- The lowest random number
SET @Upper = (SELECT max(actor_id) FROM dbo.actor) ---- The highest random number
SELECT @Del1 = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
SELECT @Del2 = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
DELETE FROM film_actor -- Delete all references
WHERE actor_id in(@Del1,@Del2);
DELETE FROM actor -- Delete actors
WHERE actor_id in(@Del1,@Del2);

PRINT 'Delete some actors from database'
GO

--Updates_Actor table
DECLARE @row INT = ROUND((((SELECT max(actor_id) FROM dbo.actor) - 1 -1) * RAND() + 1), 0);
update actor
SET first_name = first_name+'U'
WHERE actor_id= @row

GO 3

PRINT 'Update some actors'' names in database'
go


--Updates_Actor_Film: Adding some actors(30) to random film
act_film_generator:

DECLARE @Random INT;
DECLARE @RandomF INT;
DECLARE @Upper INT;
DECLARE @Lower INT;
DECLARE @UpperF INT;
DECLARE @i INT=1;

  ---- This will create a random number between 1 and count of rows in table Actor
SET @Lower = 1 ---- The lowest random number
SET @Upper = (SELECT max(actor_id) FROM dbo.actor) ---- The highest random number(count of actors in table Actor)
SET @UpperF = (SELECT max(film_id) FROM dbo.film); ---- The highest random number(count of actors in table Film)

	while @i< 2-- Adding 2 random actors to random film
		begin
			SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0);			
			SELECT @RandomF = ROUND(((@UpperF - @Lower -1) * RAND() + @Lower), 0);
			
			-- check constraINTs
			if(@RandomF IN (SELECT film_id FROM film_actor WHERE actor_id = @Random)) GOTO act_film_generator;
			if(@Random NOT IN (SELECT actor_id  FROM actor)) GOTO act_film_generator;
			if(@Random IN (SELECT actor_id FROM film_actor WHERE film_id = @RandomF)) GOTO act_film_generator;
			if(@RandomF NOT IN (SELECT film_id  FROM film)) GOTO act_film_generator;

			-- INSERT checked value
			INSERT INTO film_actor(film_id, actor_id)
				VALUES (@RandomF, @Random);
			SET @i= @i+1
		end
PRINT 'References between Films and it`s Actors ready'
GO 30


/*
-- INSERT 1 category
if((SELECT count(category_id) FROM category) < 240)
begin
DECLARE @id INT = (SELECT max(category_id) FROM category)+1;
DECLARE @name VARCHAR(20) = 'TestCategory_'+convert(VARCHAR, @id);

INSERT INTO category(category_id, name)
	VALUES(@id, @name);
end
go

--DELETE category-- 
del_cat_generator:
DECLARE @row INT = ROUND((((SELECT max(category_id) FROM dbo.category) - 1 -1) * RAND() + 1), 0);
if(@row not in (SELECT category_id FROM category)) GOTO del_cat_generator;

DELETE FROM film_category
WHERE category_id=@row;
PRINT 'DELETE category''s refereces with film'
DELETE FROM category
WHERE category_id=@row;
PRINT 'DELETE category FROM table category'
go

--Update category--
-- Changing category's name
upd_cat_generator:
DECLARE @row INT = ROUND((((SELECT max(category_id) FROM dbo.category) - 1 -1) * RAND() + 1), 0);
if(@row not in (SELECT category_id FROM category)) GOTO upd_cat_generator;

update category
SET name = substring(name,1, len(name)-2) + 'up'
WHERE category_id=@row;
PRINT 'Update category'
go
*/


-- Adding to some films several categories
fc_generator:
DECLARE @film INT = ROUND((((SELECT max(film_id) FROM dbo.film) - 1 -1) * RAND() + 1), 0);
if(@film not in(SELECT film_id FROM film)) GOTO fc_generator;


DECLARE @category INT = ROUND((((SELECT max(category_id) FROM dbo.category) - 1 -1) * RAND() + 1), 0);
if(@category not in (SELECT category_id FROM category)) GOTO fc_generator;

if(@category in (SELECT category_id FROM film_category WHERE film_id = @film)) GOTO fc_generator;
if(@film in (SELECT film_id FROM film_category WHERE category_id = @category)) GOTO fc_generator;

INSERT INTO film_category([film_id],[category_id])
VALUES (@film,@category);
PRINT 'Adding to some films several categories'
go 5



-- Insert 110 records into film table
-- Insert with nulls
IF((SELECT COUNT(*) FROM dbo.film)<31000)
BEGIN
film_generator:
DECLARE @key INT = (SELECT max(film_id) FROM dbo.film) + 1;
DECLARE @title VARCHAR(25) = 'TestTitle_' + convert(VARCHAR, @key);

DECLARE @category INT = ROUND((((SELECT max(category_id) FROM dbo.category) - 1 -1) * RAND() + 1), 0);

INSERT INTO film(film_id, title, language_id, rental_duration, rental_rate, replacement_cost)
	VALUES(@key, @title, 1, 11, 4, 11);
INSERT INTO inventory(inventory_id, film_id, store_id)
	VALUES((SELECT max(inventory_id) FROM inventory)+1, @key, 2);
INSERT INTO film_category(film_id, category_id)
	VALUES(@key, @category);
END
go 20

-- Insert withot nulls
IF((SELECT COUNT(*) FROM dbo.film)<31000)
BEGIN
DECLARE @key INT = (SELECT max(film_id) FROM dbo.film) + 1;
DECLARE @title VARCHAR(25) = 'TestTitle_' + convert(VARCHAR, @key);

DECLARE @category INT = ROUND((((SELECT max(category_id) FROM dbo.category) - 1 -1) * RAND() + 1), 0);

INSERT INTO film(film_id, title, language_id, original_language_id, rental_duration, rental_rate, replacement_cost)
	VALUES(@key, @title, 1, 3, 11, 4, 11);
INSERT INTO inventory(inventory_id, film_id, store_id)
	VALUES((SELECT max(inventory_id) FROM inventory)+1, @key, 2);
INSERT INTO film_category(film_id, category_id)
	VALUES(@key, @category);
END
GO 90


--DELETE few records from film table
del_film_generator:
DECLARE @film INT = ROUND((((SELECT max(film_id) FROM dbo.film) - 1 -1) * RAND() + 1), 0);
if(@film not in (SELECT film_id  FROM film)) GOTO del_film_generator;

DELETE FROM film_actor --DELETE references with actors
WHERE film_id=@film;

DELETE FROM payment
WHERE rental_id in(SELECT rental_id
			FROM rental 
			WHERE inventory_id in (SELECT inventory_id 
									FROM inventory 
						WHERE film_id=@film))

DELETE FROM rental
WHERE inventory_id in (SELECT inventory_id 
		FROM inventory 
		WHERE film_id=@film)

DELETE FROM inventory --Delete references with inventory
WHERE film_id=@film;

DELETE FROM film_category --Delete references with categories
WHERE film_id=@film;

DELETE FROM film --Delete film
WHERE film_id=@film;
PRINT'DELETE Film '
go


--Insert records into Customers table
IF((SELECT COUNT(*) FROM dbo.customer)<31000)
BEGIN
DECLARE @Random INT;
DECLARE @Upper INT;
DECLARE @Lower INT

SET @Lower = 1 ---- The lowest random number
SET @Upper = (SELECT max(customer_id) FROM dbo.customer) ---- The highest random number
DECLARE @row INT = (SELECT max(customer_id) FROM dbo.customer) + 1; -- number of inserting row

cust_generator:
SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0);
if(@Random not in (SELECT customer_id FROM customer)) GOTO cust_generator;

DECLARE @name VARCHAR(25) = (SELECT first_name FROM customer WHERE customer_id=@Random);
SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
if(@Random not in (SELECT customer_id FROM customer)) GOTO cust_generator;
DECLARE @lname VARCHAR(25) = (SELECT last_name FROM customer WHERE customer_id=@Random);

adr_mark:
DECLARE @address INT = ROUND((((SELECT max(address_id) FROM dbo.address) - 1 -1) * RAND() + 1), 0);
if(@address not in (SELECT address_id FROM [address])) GOTO adr_mark;

store_mark:
DECLARE @store INT =ROUND((((SELECT max(store_id) FROM dbo.store) - 1 -1) * RAND() + 1), 0);
if(@store not in (SELECT store_id FROM store)) GOTO store_mark;

INSERT INTO customer(customer_id, store_id, first_name, last_name, address_id,active, create_date)
	VALUES(@row, @store, @name, @lname, @address, 1, GETDATE());
END
go 40
PRINT 'INSERT 40 Customers'
go

--update Customer--
cupd_generator:
DECLARE @customer INT = ROUND((((SELECT max(customer_id) FROM dbo.customer) - 1 -1) * RAND() + 1), 0);
if(@customer not in (SELECT customer_id FROM customer)) GOTO cupd_generator;
update customer
SET first_name = substring(first_name,1,len(first_name)-2) + 'ev'
WHERE customer_id=@customer;
go 2
PRINT 'Update Customers names &lnames'
go

--DELETE customers--
del_cust_generator:
DECLARE @customer INT = ROUND((((SELECT max(customer_id) FROM dbo.customer) - 1 -1) * RAND() + 1), 0);
if(@customer not in (SELECT customer_id FROM customer)) GOTO del_cust_generator;

DELETE FROM payment 
WHERE customer_id in (SELECT customer_id FROM customer
WHERE customer_id=@customer);
DELETE FROM rental 
WHERE customer_id in (SELECT customer_id FROM customer
WHERE customer_id=@customer);
DELETE FROM customer
WHERE customer_id=@customer;
go 2
PRINT 'Delete 2 customers'
go



--Insert Staff
IF((SELECT COUNT(*) FROM dbo.staff)<230)
BEGIN
DECLARE @row INT = (SELECT max(staff_id) FROM dbo.staff) + 1;
DECLARE @Random INT;
DECLARE @Upper INT;
DECLARE @Lower INT

SET @Lower = 1 -- The lowest random number
SET @Upper = (SELECT max(customer_id) FROM dbo.customer) -- The highest random number

sname_generator:
SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
DECLARE @name VARCHAR(20) = (SELECT first_name FROM customer WHERE customer_id=@Random); -- SET name

if(@Random not in (SELECT customer_id FROM customer)) GOTO sname_generator;

SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
DECLARE @lname VARCHAR(20) = (SELECT last_name FROM customer WHERE customer_id=@Random);

if(@Random not in (SELECT customer_id FROM customer)) GOTO sname_generator;

sadr_generator:
DECLARE @address INT =ROUND((((SELECT max(address_id) FROM dbo.address) - 1 -1) * RAND() + 1), 0);
if(@address not in (SELECT address_id FROM [address])) GOTO sadr_generator;

staff_store_generator:
DECLARE @store INT =ROUND((((SELECT max(store_id) FROM dbo.store) - 1 -1) * RAND() + 1), 0);
if(@store not in (SELECT store_id FROM store)) GOTO staff_store_generator;

INSERT INTO staff(staff_id, store_id, first_name, last_name,username, address_id,active)
	VALUES(@row, @store, @name, @lname, @lname, @address, 1);
END
GO 3
PRINT 'Insert 3 Staff'
go

--Update Staff--
supd_generator:
DECLARE @staff INT = ROUND((((SELECT max(staff_id) FROM dbo.staff) - 1 -1) * RAND() + 1), 0);
if(@staff not in (SELECT staff_id FROM staff)) GOTO supd_generator;

update staff
SET first_name = substring(first_name, 1, len(first_name)-2) + '_u'
WHERE staff_id=@staff;
go 2
PRINT 'update Staffs'' names &lnames'
go

--DELETE Staff--
/*DECLARE @staff INT =ROUND((((SELECT max(staff_id) FROM dbo.staff) - 1 -1) * RAND() + 1), 0);
DELETE FROM staff
WHERE staff_id=@staff;
PRINT 'DELETE staff'
go*/


-- Insert rentals
-- without null return date
IF((SELECT COUNT(*) FROM dbo.rental)<30000)
BEGIN
cust_generator:
DECLARE @customer INT = ROUND((((SELECT max(customer_id) FROM dbo.customer) - 1 -1) * RAND() + 1), 0);
if(@customer not in (SELECT customer_id FROM customer)) GOTO cust_generator

inv_generator:
DECLARE @inventory INT= ROUND((((SELECT max(inventory_id) FROM dbo.inventory) - 1 -1) * RAND() + 1), 0);
if(@inventory not in (SELECT inventory_id FROM inventory)) GOTO inv_generator;

DECLARE @film INT = (SELECT film_id FROM inventory WHERE inventory_id = @inventory)


DECLARE @cKey INT = (SELECT CustomerKey FROM sakilaDW.dbo.DimCustomer WHERE CustomerAltKey = @customer);
DECLARE @fKey INT = (SELECT FilmKey FROM sakilaDW.dbo.DimFilm WHERE FilmAltKey = @film)

if(
(@cKey) is null
or
(@fKey) is null) GOTO cust_generator;

DECLARE @rand_seed INT = convert(INT, RAND(convert(varbinary, newid()))*1000);
DECLARE @rentDate datetime = dateadd(day, -@rand_seed, getdate());
DECLARE @date date = convert(date, @rentDate)

if(
(SELECT count(RentalId)
FROM sakilaDW.dbo.FactRental
WHERE CustomerKey = @cKey and FilmKey = @fKey and RentalDateKey = convert(INT, convert(VARCHAR(10), @date, 112))) > 0) 
begin
	GOTO cust_generator;
end

DECLARE @id INT = (SELECT max(rental_id) FROM rental) +1;

DECLARE @s_id INT;

if(@id%2=0) SET @s_id = 1
else SET @s_id = 2;


INSERT INTO rental(rental_id, rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES(@id, @rentDate, @inventory, @customer, GETDATE(), @s_id)
INSERT INTO payment(payment_id, customer_id, staff_id, rental_id, amount, payment_date)
	VALUES((SELECT max(payment_id) FROM payment)+1, @customer, @s_id, @id, 7.7, getdate());
END
go 200


-- with nulls
IF((SELECT COUNT(*) FROM dbo.rental)<30000)
BEGIN
cust_generator:
DECLARE @customer INT = ROUND((((SELECT max(customer_id) FROM dbo.customer) - 1 -1) * RAND() + 1), 0);
if(@customer not in (SELECT customer_id FROM customer)) GOTO cust_generator

inv_generator:
DECLARE @inventory INT= ROUND((((SELECT max(inventory_id) FROM dbo.inventory) - 1 -1) * RAND() + 1), 0);
if(@inventory not in (SELECT inventory_id FROM inventory)) GOTO inv_generator;

DECLARE @film INT = (SELECT film_id FROM inventory WHERE inventory_id = @inventory)


DECLARE @cKey INT = (SELECT CustomerKey FROM sakilaDW.dbo.DimCustomer WHERE CustomerAltKey = @customer);
DECLARE @fKey INT = (SELECT FilmKey FROM sakilaDW.dbo.DimFilm WHERE FilmAltKey = @film)

if(
(@cKey) is null
or
(@fKey) is null) GOTO cust_generator;

DECLARE @rand_seed INT = convert(INT, RAND(convert(varbinary, newid()))*1000);
DECLARE @rentDate datetime = dateadd(day, -@rand_seed, getdate());
DECLARE @date date = convert(date, @rentDate)

if(
(SELECT count(*)
FROM sakilaDW.dbo.FactRental
WHERE CustomerKey = @cKey and FilmKey = @fKey and RentalDateKey = convert(INT, convert(VARCHAR(10), @date, 112))) > 0) 
begin
	GOTO cust_generator;
end

DECLARE @id INT = (SELECT max(rental_id) FROM rental) +1;

DECLARE @s_id INT;

if(@id%2=0) SET @s_id = 1
else SET @s_id = 2;

INSERT INTO rental(rental_id, rental_date, inventory_id, customer_id, staff_id)
VALUES(@id, @rentDate, @inventory, @customer, @s_id)
INSERT INTO payment(payment_id, customer_id, staff_id, rental_id, amount, payment_date)
	VALUES((SELECT max(payment_id) FROM payment)+1, @customer, @s_id, @id, 7.7, getdate());
END
go 50


--Update rental-- Changing return date
update rental
SET return_date = GETDATE()
WHERE rental_id IN (SELECT top 15 rental_id FROM rental WHERE return_date IS NULL)
go 
PRINT 'Update rental with null'
go
